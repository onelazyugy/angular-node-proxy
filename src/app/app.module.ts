import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BackendService } from './service/backend.service';
import { StoreModule } from './store/store.module';
import { HomeModule } from './home/home.module';
import { CatalogModule } from './catalog/catalog.module';
import { CartModule } from './cart/cart.module';
import { CartService } from './service/cart.service';
import { Error404Module } from './error404/error404.module';
import { LoginModule } from './login/login.module';
import { AuthService } from './service/authentication.service';
import { AuthGuard } from './service/authentication-guard.service';
import { StoreService } from './service/store.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LoginModule,
    StoreModule,
    HomeModule,
    CatalogModule,
    CartModule,
    Error404Module
  ],
  providers: [BackendService, CartService, AuthService, StoreService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
