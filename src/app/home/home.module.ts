import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { AppAngularMaterialModule } from '../app-angular-material.module';

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        AppAngularMaterialModule
    ]
})
export class HomeModule {}