import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { HealthCheck } from '../model/healthcheck.model';
import { BackendService } from '../service/backend.service';
import { CartService } from '../service/cart.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../service/authentication.service';
import { AddItemToCartResponse } from '../model/AddItemToCartResponse';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  healthCheck:HealthCheck;
  healthCheckMessage:string;
  subscriptionBackend: Subscription;
  subscriptionCartService: Subscription;
  itemInCart: number;
  items = [
    {id: 1, itemName: 'shirt', itemPrice: '10.99', itemDescription: 'this is a cool shirt'},
    {id: 2, itemName: 'pant', itemPrice: '19.99', itemDescription: 'this is a cool pant'},
    {id: 3, itemName: 'purse', itemPrice: '7.99', itemDescription: 'this is a cool purse'},
    {id: 4, itemName: 'shoe', itemPrice: '190.99', itemDescription: 'this is a cool shoe'}
  ];
  isItemSuccessfullyAdded: boolean = false;
  isAddingItemToCart: boolean = false;
  isError: boolean = false;
  message: string = '';
  addItemToCartResponse: AddItemToCartResponse;

  constructor(private router: Router, private backendService: BackendService,
  private cartService: CartService, private authService: AuthService) { }

  ngOnInit() {
    console.log(this.items);
    this.subscriptionBackend = this.backendService.getHealthStatusMessage().subscribe(heathStatus => {
      this.healthCheckMessage = heathStatus;
    });
    this.subscriptionCartService = this.cartService.getCount().subscribe(count=> {
      this.itemInCart = count;
    });
  }

  navigateHomeClicked = () => {
    this.router.navigate(['']);
  }

  navigateStoreClicked = () => {
    this.router.navigate(['store']);
  }

  navigateCatalogClicked = () => {
    this.router.navigate(['catalog']);
  }

  navigateCartClicked = () => {
    this.router.navigate(['cart']);
  }

  healthCheckClicked = () => {
    this.backendService.healthCheck().subscribe(resp => {
      console.log(resp.body);
      this.healthCheck = resp.body;
      this.healthCheckMessage = this.healthCheck.status;
      // emit here
      this.backendService.sendHealthStatusMessage(this.healthCheckMessage);
    });
  }

  addItem = (item) => {
    this.cartService.setCount(1);
    this.subscriptionCartService = this.cartService.getCount().subscribe(count=>{
      this.itemInCart = count;
    });

    //add item to card and send to backend
    this.isAddingItemToCart = true;
    this.cartService.addItemToCart(item).subscribe(response => {
      this.addItemToCartResponse = response;
      if(this.addItemToCartResponse.status.statusCd == 200) {
        this.isItemSuccessfullyAdded = true;
        this.isAddingItemToCart = false;
        this.message = `${this.addItemToCartResponse.status.status}`
      } 
    }, error => {
      this.isError = true;
      this.isAddingItemToCart = false;
      this.isItemSuccessfullyAdded = false;
      this.message = `[${error.statusText}]`;
      console.log(error);
    }, () => {
      
    });
  }

  onLogout = () => {
    this.authService.logout();
    this.router.navigate(['login']);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscriptionBackend.unsubscribe();
    this.subscriptionCartService.unsubscribe();
  }
}
