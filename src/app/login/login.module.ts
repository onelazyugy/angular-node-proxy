import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { AppAngularMaterialModule } from '../app-angular-material.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [AppAngularMaterialModule, FormsModule]
})
export class LoginModule {};