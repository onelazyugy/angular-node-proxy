import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm', {static: false}) signupForm: NgForm;
  user = {
    username: '',
    password: ''
  };

  constructor(private router: Router, private authService: AuthService ) { }

  ngOnInit() {
    this.setDefaultLoginFormValue();
  }

  setDefaultLoginFormValue() {
    this.user = {
      username: 'default',
      password: 'default'
    }
  }

  onSubmit() {
    console.log(this.signupForm);
    this.user.username = this.signupForm.value.userData.username;
    this.user.password = this.signupForm.value.userData.password;
    this.signupForm.reset();
    this.authService.login();
    this.router.navigate(['']);
  }
}
