import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class AuthService {
    loggedIn:boolean;

    constructor() {}

    //fake backend call 
    isAuthenticated() {
        const promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.loggedIn);
            }, 100);
        });
        return promise;
    }

    login() {
        //https://jasonwatmore.com/post/2019/06/22/angular-8-jwt-authentication-example-tutorial
        this.loggedIn = true;
    }

    logout() {
        this.loggedIn = false;
    }
}