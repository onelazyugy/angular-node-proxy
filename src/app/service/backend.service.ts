import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { HealthCheck } from '../model/healthcheck.model';

@Injectable()
export class BackendService {
    healthStatus = '';
    // private subject = new Subject();
    message: string;

    constructor(private http: HttpClient){}

    sendHealthStatusMessage(message) {
        this.message = message;
    }

    getHealthStatusMessage() {
        return of(this.message);
    }

    healthCheck(): Observable<HttpResponse<HealthCheck>> {
        console.log('health check called...');
        const url = 'http://localhost:3001/ecommerce/actuator/health';
        return this.http.get<HealthCheck>(url, {observe: 'response'});
    }
}