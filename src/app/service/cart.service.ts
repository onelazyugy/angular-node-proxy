import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AddItemToCartResponse } from '../model/AddItemToCartResponse';
import { Item } from '../model/Item';

@Injectable()
export class CartService {
    count: number = 0;

    constructor(private http: HttpClient){}

    setCount(item) {
        this.count = (this.count + item);
    }

    getCount() {
        return of(this.count);
    }

    addItemToCart(item: Item) {
        const url = 'http://localhost:3001/ecommerce/api/v1/add-item-to-cart';
        return this.http.post<AddItemToCartResponse>(url, JSON.stringify(item));
    }
}