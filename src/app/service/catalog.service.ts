import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Catalog } from '../model/catalog.model';

@Injectable({
    providedIn: 'root'
  })
export class CatalogService {
    catalog: Catalog = {
        "totalItem": 0,
        "items": []
    }
    private catalogSubject$ = new BehaviorSubject<Catalog>(this.catalog);
    catalogChange$ = this.catalogSubject$.asObservable();
    
    constructor(private http: HttpClient){}

    fetchCatalogDetials(): Observable<HttpResponse<Catalog>> {
        const url = 'http://localhost:3001/ecommerce/api/v1/catalog/details';
        return this.http.get<Catalog>(url, {observe: 'response'});

        // return this.http.get(url).pipe(map((res:Response) => res.json())
        // .catch((error:any) => Observable.throw(error.json().error || 'Server error')));

        // return this.http.get(url).map((res:Response) => res.json())
        // .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
        
    }

    emitCatalog(catalog: Catalog) {
        this.catalogSubject$.next(catalog);
    }
}