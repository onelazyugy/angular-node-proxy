import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { StoreComponent } from './store/store.component';
import { CatalogComponent } from './catalog/catalog.component';
import { CartComponent } from './cart/cart.component';
import { Error404Component } from './error404/error404.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './service/authentication-guard.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard], 
    component: HomeComponent,
    
  },
  {path: 'login', component: LoginComponent},
  { 
    path: 'store', 
    canActivate: [AuthGuard],
    component: StoreComponent
  },
  {
    path: 'catalog', 
    canActivate: [AuthGuard],
    component: CatalogComponent
  },
  {
    path: 'cart', 
    canActivate: [AuthGuard],
    component: CartComponent
  },
  {
    path: 'not-found', 
    component: Error404Component, 
    data: {message: 'page not found!'}
  },
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
