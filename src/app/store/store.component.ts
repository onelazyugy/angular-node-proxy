import { Component, OnInit, OnDestroy } from '@angular/core';
import { BackendService } from '../service/backend.service';
import { Subscription } from 'rxjs';
import { StoreService } from '../service/store.service';
import { Store } from '../model/store.model';
import { CatalogService } from '../service/catalog.service';
import { Catalog } from '../model/catalog.model';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit, OnDestroy {
  healthCheckMessage:string;
  backEndSubscription: Subscription;
  storeSubscription: Subscription;
  storeData: Store;
  catalog: Catalog;
  catalogSubscription: Subscription;

  constructor(private backendService: BackendService, private storeService: StoreService, private catalogService: CatalogService) {

  }

  ngOnInit() {
    this.backEndSubscription = this.backendService.getHealthStatusMessage().subscribe(heathStatus => {
      this.healthCheckMessage = heathStatus;
    });
    this.storeSubscription = this.storeService.fetchStoreDetials().subscribe(response => {
      if(response.status == 200 && response.statusText == 'OK') {
        this.storeData = response.body
      }
    }, error =>{
      console.error(error);
    }, () => {
    });

    // get the catalog data by subscribing to catalogService
    this.catalogSubscription = this.catalogService.catalogChange$.subscribe(catalog => {
      this.catalog = catalog;
    })
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.backEndSubscription.unsubscribe();
    this.storeSubscription.unsubscribe();
    this.catalogSubscription.unsubscribe();
  }
}
