import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StoreComponent } from './store.component';
@NgModule({
    declarations: [
        StoreComponent
    ],
    imports: [
        CommonModule
    ]
})
export class StoreModule {}