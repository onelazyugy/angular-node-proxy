import { NgModule } from '@angular/core';
import { CartComponent } from './cart.component';

@NgModule({
    declarations: [
        CartComponent
    ],
    imports: [

    ]
})
export class CartModule {}
