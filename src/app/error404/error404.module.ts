import { NgModule } from '@angular/core';
import { Error404Component } from './error404.component';

@NgModule({
    declarations: [
        Error404Component
    ],
    imports: []
})
export class Error404Module {}