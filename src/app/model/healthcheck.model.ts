export class HealthCheck {
    public version:string;
    public year:string;
    public message:string;
    public status:string;

    constructor(version:string, year:string, message:string, status:string) {
        this.version = version;
        this.year=year;
        this.message=message;
        this.status=status;
    }
}