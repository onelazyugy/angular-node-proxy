import { Item } from './Item';

export class Status {
    public status: string;
    public statusCd: number;
    public transId: string;
}