export class Store {
    public totalEmployee: number;
    public items: any;
    public storeHour: string;
    public totalItem: number;

    constructor(totalEmployee: number, items: any, storeHour: string, totalItem: number) {
        this.totalEmployee = totalEmployee;
        this.items = items;
        this.storeHour = storeHour;
        this.totalItem = totalItem;
    }
}