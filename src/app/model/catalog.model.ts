export class Catalog {
    public totalItem: number;
    public items: [];

    constructor(totalItem: number, items: []) {
        this.items = items;
        this.totalItem = totalItem;
    }
}