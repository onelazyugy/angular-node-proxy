export class Item {
    public id: number;
    public itemName: string;
    public itemPrice: string;
    public itemDescription: string;
}