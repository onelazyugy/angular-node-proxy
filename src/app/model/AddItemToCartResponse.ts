import { Item } from './Item';
import { Status } from './Status';

export class AddItemToCartResponse {
    public status: Status;
    public item: Item;
}