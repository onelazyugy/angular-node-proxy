import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CatalogComponent } from './catalog.component';
import { ItemComponent } from './item/item.component';
import { AppAngularMaterialModule } from '../app-angular-material.module';


@NgModule({
    declarations: [
        CatalogComponent,
        ItemComponent
    ],
    imports: [
        AppAngularMaterialModule, CommonModule
    ]
})
export class CatalogModule {}