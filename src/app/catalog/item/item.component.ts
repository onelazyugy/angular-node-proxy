import { Component, OnInit, OnDestroy } from '@angular/core';
import { Catalog } from 'src/app/model/catalog.model';
import { CatalogService } from 'src/app/service/catalog.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit, OnDestroy {
  catalog: Catalog;
  catalogSubscription: Subscription;

  constructor(private catalogService: CatalogService) { }

  ngOnInit() {
    //listen to a change in catalog
    this.catalogSubscription = this.catalogService.catalogChange$.subscribe(catalog => {
      this.catalog = catalog;
    });
  }

  ngOnDestroy() { 
    this.catalogSubscription.unsubscribe();
  }
}
