import { Component, OnInit, OnDestroy } from '@angular/core';
import { BackendService } from '../service/backend.service';
import { Subscription } from 'rxjs';
import { CatalogService } from '../service/catalog.service';
import { Catalog } from '../model/catalog.model';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit, OnDestroy {
  healthCheckMessage:string;
  subscription: Subscription;

  constructor(private backendService: BackendService, private catalogService: CatalogService) { }

  ngOnInit() {
    this.subscription = this.backendService.getHealthStatusMessage().subscribe(healthStatusMsg => {
      this.healthCheckMessage = healthStatusMsg
    });
  }

  getItemsClicked() {
    this.catalogService.fetchCatalogDetials().subscribe(response =>{
      const catalog: Catalog = response.body;
      //send it to whichever component is listening to this event
      this.catalogService.emitCatalog(catalog);
    }, error =>{
      console.error(error);
    }, () =>{

    })
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

}
